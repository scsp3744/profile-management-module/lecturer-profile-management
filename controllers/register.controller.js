﻿var express = require('express');
var router = express.Router();
var request = require('request');
var config = require('config.json');

router.get('/', function (req, res) {
    res.render('register');
});

router.post('/', function (req, res) {
    // register using api to maintain clean separation between layers
    request.post({
        url: config.apiUrl + '/users/register',
        form: req.body,
        json: true
    }, function (error, response, body) {
        if (error) {
            return res.render('register', { error: 'An error occurred' });
        }

        if (response.statusCode !== 200) {
            return res.render('register', {
                error: response.body,
                staffid: req.body.staffid,
                name: req.body.name,
                ic: req.body.ic,
                address: req.body.address,
                gender: req.body.gender,
                username: req.body.username,
                nation: req.body.nation,
                race: req.body.race,
                religion: req.body.religion,
                marriage: req.body.marriage,
                phone: req.body.phone,
                disability: req.body.disability,
                status: req.body.status,
                year: req.body.year,

                username: req.body.username,
                email: req.body.email,
                secemail: req.body.secemail,
                
                fac: req.body.fac,
                bahagian: req.body.bahagian,
                taraf: req.body.taraf,
                position: req.body.position,
                jgiliran: req.body.jgiliran,
                kod: req.body.kod,
                cat: req.body.cat,
                unit: req.body.unit,
                ptjra: req.body.ptjra,
                ptjcoe: req.body.ptjcoe,
                rg: req.body.rg,
                kodclass: req.body.kodclass,
                tarikh: req.body.tarikh,

                ic: req.body.ic,
                iclama: req.body.iclama,
                passport: req.body.passport

            });
        }

        // return to login page with success message
        req.session.success = 'Registration successful';
        return res.redirect('/login');
    });
});

module.exports = router;